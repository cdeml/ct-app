//
//  WebAPITutorialAppDelegate.h
//  WebAPITutorial
//
//  Created by chris on 17.11.13.
//  Copyright (c) 2013 CT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebAPITutorialAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
