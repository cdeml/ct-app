//
//  main.m
//  WebAPITutorial
//
//  Created by chris on 17.11.13.
//  Copyright (c) 2013 CT. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WebAPITutorialAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([WebAPITutorialAppDelegate class]));
    }
}
